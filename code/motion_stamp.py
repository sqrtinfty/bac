#import library
from SimpleCV import *
from time import sleep
from PIL import Image, ImageDraw, ImageFont

#only first at start
cam=Camera()
lastImg=cam.getImage()

#rest of program in loop
while 1:
    #using SimpleCV to detect motion
    newImg=cam.getImage()
    trackImg=newImg-lastImg
    blobs=trackImg.findBlobs(100)

    #if motion is detected
    if blobs:
        #print to consol and save a temperary picture
        print "motion detected " + time.ctime()
        newImg.save("temp.jpg")

        #use PIL to add a timestamp and save final picture
        #get image
        base=Image.open("temp.jpg").convert("RGBA")
        #new blank image for text
        txt=Image.new("RGBA", base.size, (255,255,255,0))
        #select font
        fnt=ImageFont.truetype("Verdana.ttf",40)
        #drawing context
        d=ImageDraw.Draw(txt)
        #draw text, full opaticy
        d.text((10,10), time.ctime(), font=fnt, fill=(255,255,255,255))
        out= Image.alpha_composite(base, txt)
        out.save("out.jpg")

    #get ready for a new loop after 0.3 sec
    lastImg=newImg
    sleep(0.3)

#!/bin/bash
#chmod +x rpi3bac.sh
#sudo ./rpi3bac.sh

#installing packages
sudo apt-get install luvcview idle python-flask imagemagick python-opencv 
sudo apt-get install python-scipy python-numpy python-zbar libjpeg-dev python-imaging 
sudo apt-get install python-pygame nodered npm python-pip
sudo pip install https://github.com/ingenuitas/SimpleCV/zipball/master
sudo pip install elaphe
sudo npm install node-red-dashboard

#changing versionnumber on node-red packagemanager
sudo npm i -g npm@2.x

#runing node-red as a service
sudo systemctl enable nodered.service

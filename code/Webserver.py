#import library
from flask import Flask,send_file,request,render_template, Response

#creating a flaskserver with information about folderstructure
app = Flask(__name__, static_url_path = "/templates", static_folder = "templates")

#root page on server runs "index.html" saved in /templates folder
@app.route('/')
def index():
    return render_template('index.html')

#Everyone is allowed to access
if __name__ == '__main__':
    app.run(host='0.0.0.0') 


